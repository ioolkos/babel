

# The babel application #


## Modules ##


<table width="100%" border="0" summary="list of modules">
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel.md" class="module">babel</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_app.md" class="module">babel_app</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_config.md" class="module">babel_config</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_consistent_hashing.md" class="module">babel_consistent_hashing</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_counter.md" class="module">babel_counter</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_crdt.md" class="module">babel_crdt</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_flag.md" class="module">babel_flag</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_hash_partitioned_index.md" class="module">babel_hash_partitioned_index</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_index.md" class="module">babel_index</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_index_collection.md" class="module">babel_index_collection</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_index_partition.md" class="module">babel_index_partition</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_index_utils.md" class="module">babel_index_utils</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_key_value.md" class="module">babel_key_value</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_manager.md" class="module">babel_manager</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_map.md" class="module">babel_map</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_set.md" class="module">babel_set</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_simple_index.md" class="module">babel_simple_index</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_sup.md" class="module">babel_sup</a></td></tr>
<tr><td><a href="https://gitlab.com/leapsight/babel/blob/develop/doc/babel_utils.md" class="module">babel_utils</a></td></tr></table>

